@extends('layouts.app')
@section('title','Hotel | Registration')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hotel Registration</div>

                <div class="panel-body">
                     <form class="form-horizontal" role="form" method="POST" onsubmit="return validatehotelform();" action="{{ url('admin/hotelRegisteration') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Hotel Name</label>

                            <div class="col-md-6">
                                <input id="hotel_name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="text" class="col-md-4 control-label">Hotel Address</label>

                            <div class="col-md-6">
                                <input id="hotel_address" type="text" class="form-control" name="address" value="{{ old('address') }}">

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
                            <label for=" " class="col-md-4 control-label">Mobile No.</label>

                            <div class="col-md-6">
                                <input id="mobile_no" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">

                                @if ($errors->has('mobile'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
 
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register Hotel
                                </button>
                            </div>
                        </div>
                         <span class="error_class"></span>    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
