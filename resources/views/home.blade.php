@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Hotel List</div>

                <div class="panel-body">
                <table class="table table-striped task-table">

                                <!-- Table Headings -->
                                <thead>
                                    <th>Hotel Name</th>
                                    <th>Address</th>
                                    <th>Mobile</th>
                                </thead>
                                 
                                    @foreach ($hotels as $hotel)
                                        <tr>
                                            <!-- Task Name -->
                                            <td class="table-text">
                                                <div>{{ $hotel->name }}</div>
                                            </td>
                                            <td class="table-text">
                                                <div>{{ $hotel->address }}</div>
                                            </td>
                                            <td class="table-text">
                                                <div>{{ $hotel->mobile }}</div>
                                            </td>
                                             <td class="table-text">
                                                <div><a href="{{ url('editcomment', array()) }}">Comment</a> </div>
                                            </td>

                                            
                                  
                                        </tr>
                                    @endforeach
                                <tbody>
                            </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
