<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use App\Hotel;
use DB;
use App\Http\Controllers\Controller;


class HotelController extends Controller
{
     public function hotel()
    {
         
        return view('hotel');
    }

       public function hotelRegisteration(Request $request)
    {	
    	/* $this->Validate($request,[
		  'name'  => 'required|max:150', 
		  'address'  => 'required|max:150',
		  'mobile'  => 'required|regex:/^(\+\d{1,3}[- ]?)?\d{10}$/',


		  ]);*/

		    $validator = Validator::make($request->all(), [
				'name'  => 'required|max:150', 
		 		'address'  => 'required|max:150',
		  		'mobile'  => 'required|regex:/^(\+\d{1,3}[- ]?)?\d{10}$/',
			]);

				  if ($validator->fails()) {
					return redirect()->back()
					->withInput()
					->withErrors($validator);
			}
         		$hotel = new Hotel;
				$hotel->name = $request->name;
				$hotel->address = $request->address;
				$hotel->mobile = $request->mobile;
				$hotel->save();

			return redirect()->back();
			//return redirect('/');       
	}

	  public function hotelView()
    {
        $hotels = DB::table('hotels')->get();
        return view('home',['hotels' => $hotels]);
    }




}
